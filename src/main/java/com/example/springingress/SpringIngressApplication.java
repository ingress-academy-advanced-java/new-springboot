package com.example.springingress;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringIngressApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringIngressApplication.class, args);
    }

}
