package com.example.springingress.service;


import com.example.springingress.dto.request.UserRequest;
import com.example.springingress.dto.response.UserResponse;
import com.example.springingress.model.User;

import java.util.List;

public interface UserService {
    User save(UserRequest userRequest);
    UserResponse getById(String id);
    List<UserResponse> getAllUsers();
}
