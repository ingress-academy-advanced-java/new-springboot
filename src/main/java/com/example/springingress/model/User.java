package com.example.springingress.model;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;


@Entity
@Table(name = "User")
@FieldDefaults(level = AccessLevel.PUBLIC)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    String username;
    String email;
    String password;
}
